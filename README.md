## About Me
Hi there! I'm a graduate of the network engineering and security course at Mohawk College, with a little experience with basic coding. All of my projects and exercises will be stored here, much of it private and archival in nature. Things like my scripts repo, however, will be left public in the hopes that someone might find them useful.<br>

## What I'm up to now
💻 <b>Currently working on: </b>Organizing my projects in GitLab.<br>
💡 <b>What I'm learning about: </b>Network security<br>
📚 <b>What I'm reading: </b>Battle Angel Alita<br>
🎮 <b>What I'm playing: </b>Factorio<br>
📺 <b>What I'm watching: </b>Doctor Who<br>
🎵 <b>I've been listening to: </b>Taylor Swift<br>
🎙️ <b>Current favourite podcast: </b> <a href="https://www.dungeonsanddaddies.com/">Dungeons & Daddies</a><br>

## Useful Links
<a href="https://www.apple.com/swift/playgrounds/">Swift Playgrounds</a> - The best way to start coding.<br>
<a href="http://compsci.ca/holtsoft/">Turing</a> - Fantastic first programming language.<br>
<a href="https://code.visualstudio.com/">Visual Studio Code</a> - Fast and simple multi-language, multi-platform IDE.<br>
<a href="https://leetcode.com">LeetCode</a> - Coding exercises to keep you sharp.<br>
